<?php
$autoload = __DIR__ . "/vendor/autoload.php";
if (is_file($autoload))
    require_once $autoload;
$utilsphp = __DIR__ . "/src/pw/Utils/Utils.php";
    require_once $utilsphp;
    
use pw\Utils\Utils;

if ($argc < 3) {
    echo "Usage: {$argv[0]} <command> <instr>\n";
    exit;
}

echo "Command '{$argv[1]}' " . (Utils::commandExists($argv[1], $argv[2]) ? "exists." : "does not exist.");

Utils::dump ([1,2,3], ["a"=>null]);