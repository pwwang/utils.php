<?php
namespace pw\Utils;

class Utils {
    
    /**
	 * Check whether a command exists
	 * @var $cmd The command, quote if needed
     * @var $instr The included output
     * @return true if exists, else false
	 */
    static public function commandExists ($cmd, $instr) {
		if (class_exists("\kamermans\Command\Command", true)) {
			$cmd = \kamermans\Command\Command::factory ($cmd . " 2>&1", true)->run ();
			return strpos ($cmd->getStdOut(), $instr) !== false;
        }
        else {
			$cmd = "$cmd 2>&1";
			return strpos (shell_exec($cmd), $instr) !== false;
        }
    }
    
    /**
     * Use kevinlebrun/colors.php if installed, otherwise return plain text.
     */
    static public function color () {
		$args = func_get_args();
		if (class_exists("\Colors\Color", true)) {
			$c = new \Colors\Color(array_shift($args));
			while (!empty($args)) {
				$style = array_shift($args);
				$c = $c -> $style;
			}
			return $c;
		} else {
			return $args[0];
		}
	}
	
	static public function dump ($var) {
		if (class_exists("\Kint", true)) {
			call_user_func_array (["\Kint", "dump"], func_get_args());
		} else {
			call_user_func_array ("var_dump", func_get_args());
		}
	}
	
	static public function mprun ($tasks, $ncpu = 16, $run = false) {
    $size       = sizeof ($tasks);
    $taskpercpu = ceil ($size/$ncpu);
    $bashfiles  = [];
    $assigned   = [];
    foreach ($tasks as $i => $task) {
      $index = floor ($i / $taskpercpu);
      if (!isset ($assigned[$index])) $assigned [$index] = [];
      
      $assigned[$index][] = $task;
    }
    
    foreach ($assigned as $i => $tasks) {
      $bashfiles[$i] = tempnam(sys_get_temp_dir(), "pw-utils-mprun-");
      file_put_contents ($bashfiles[$i], implode("\n", $tasks) . PHP_EOL);
    }
    
    foreach ($bashfiles as $bfile) {
      echo $bfile . PHP_EOL;
      if ($run)
        exec ("nohup bash $bfile > /dev/null 2>&1 &");
    }
    
  }

}